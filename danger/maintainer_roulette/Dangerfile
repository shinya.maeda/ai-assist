# frozen_string_literal: true

###
# NOTE: At the moment, only a maintainer review is required to merge an MR because this project is short-handed.

PROJECT_NAME = helper.config.project_name

MESSAGE = <<~MARKDOWN
  ## Maintainer roulette
MARKDOWN

TABLE_MARKDOWN = <<~MARKDOWN
  To spread load more evenly across eligible maintainers, Danger has picked a candidate for each
  review slot. Feel free to
  [override these selections](https://about.gitlab.com/handbook/engineering/projects/##{PROJECT_NAME})
  if you think someone else would be better-suited
  or use the [GitLab Review Workload Dashboard](https://gitlab-org.gitlab.io/gitlab-roulette/?currentProject=#{PROJECT_NAME}) to find other available maintainers.

  To read more on how to use the maintainer roulette, please take a look at the
  [Engineering workflow](https://about.gitlab.com/handbook/engineering/workflow/#basics)
  and [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html).

  Once you've decided who will review this merge request, mention them as you
  normally would! Danger does not automatically notify them for you.
MARKDOWN

TABLE_HEADER = <<~MARKDOWN
  | Maintainer |
  | ---------- |
MARKDOWN

WARNING_MESSAGE = <<~MARKDOWN
  ⚠ Failed to retrieve information ⚠
  %{warnings}
MARKDOWN

OPTIONAL_REVIEW_TEMPLATE = '%{role} review is optional'
NOT_AVAILABLE_TEMPLATE = 'No %{role} available'

def note_for_spins_role(spins, role)
  spins.each do |spin|
    note = note_for_spin_role(spin, role)

    return note if note
  end

  format(NOT_AVAILABLE_TEMPLATE, role: role)
end

def note_for_spin_role(spin, role)
  return format(OPTIONAL_REVIEW_TEMPLATE, role: role.capitalize) if spin.optional_role == role

  spin.public_send(role)&.markdown_name(author: roulette.team_mr_author)
end

def markdown_row_for_spins(spins_array)
  maintainer_note = note_for_spins_role(spins_array, :maintainer)

  "| #{maintainer_note} |"
end

def warning_list(roulette)
  roulette.warnings.map { |warning| "* #{warning}" }
end

changes = helper.changes_by_category

if changes.any?
  markdown(MESSAGE)

  warnings = warning_list(roulette)
  markdown(format(WARNING_MESSAGE, warnings: warnings.join("\n"))) unless warnings.empty?

  random_roulette_spins = roulette.spin
  rows = random_roulette_spins.map do |spin|
    markdown_row_for_spins([spin])
  end

  markdown(TABLE_MARKDOWN + TABLE_HEADER + rows.join("\n")) unless rows.empty?
end
